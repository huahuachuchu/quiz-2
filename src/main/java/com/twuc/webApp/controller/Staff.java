package com.twuc.webApp.controller;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.ResourceSupport;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Staff extends ResourceSupport {
    @Id
    private Long staffId;
    @Column(nullable = false,length = 64)
    @NotNull
    @Size(max = 64)
    private String firstName;
    @Column(nullable = false,length = 64)
    @NotNull
    @Size(max = 64)
    private String lastName;

    public Staff() {
    }

    public Staff(Long staffId,@NotNull @Size(max = 64) String firstName, @NotNull @Size(max = 64)String lastName) {
        this.staffId = staffId;
        this.firstName = firstName;
        this.lastName = lastName;
    }
    @JsonProperty("staffId")
    public Long getStaffId() {
        return staffId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
