package com.twuc.webApp.controller;

import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/api/staffs")
public class StaffController {
    @PostMapping("/{staffId}")
    public ResponseEntity addStaff(@PathVariable Long staffId){
        Staff staff = new Staff(staffId,"Rob","Hall");
        return ResponseEntity.status(HttpStatus.CREATED).contentType(MediaType.APPLICATION_JSON_UTF8).body(staff);
    }
    @GetMapping("/{staffId}")
    public ResponseEntity updateStaff(@PathVariable Long staffId,@RequestBody@Valid Staff staffs){
        Staff staff = new Staff();
        return ResponseEntity.status(HttpStatus.CREATED).contentType(MediaType.APPLICATION_JSON_UTF8).body(staff);
    }
}
